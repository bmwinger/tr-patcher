public interface ProgressNotification {
	void notifyAboutCurrentFile(String currentFile);

	void notifyAboutProgress(int percent, boolean finished);

	void notifyAboutLastObject(String object);
}

