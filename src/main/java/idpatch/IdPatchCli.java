import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import model.EsFile;
import net.sf.jsqlparser.JSQLParserException;

public class IdPatchCli implements ProgressNotification {
	private FilePatcher filePatcher;
	private List<Translation> translations;
	private int progress = 0;
	private boolean started = false;

	public IdPatchCli() throws IOException {

		URL applicationRootPathURL = getClass().getProtectionDomain().getCodeSource().getLocation();
		File applicationRootPath = new File(applicationRootPathURL.getPath());
		File file;
		if(applicationRootPath.isDirectory()){
			file = new File(applicationRootPath, "Translation.txt");
		}
		else{
			file = new File(applicationRootPath.getParentFile(), "Translation.txt");
		}

		if (!file.exists()) {
			file = new File(System.getProperty("user.dir") + File.separatorChar + "Translation.txt");

			if (!file.exists()) {
				System.out.println("The Translation.txt is not available. Please add it to the current folder and restart the program.");
				return;
			}
		}

		try {
			translations = TranslationFileReader.readTranslationsFromFile(file.getCanonicalPath());
		} catch (IOException e) {
			System.out.println("The Translation.txt could not be read. Please make sure that there are no syntax errors in the file and restart the program.");
			e.printStackTrace();
			return;
		}
		filePatcher = new FilePatcher(this);
	}

	public static void main(String[] args) {
		final List<EsFile> esFiles = new LinkedList<EsFile>();

		for (String fileName: args) {
			try {
				esFiles.add(new EsFile(fileName));
			} catch (IOException e) {
				System.out.println("An error occured while trying to read the files to be patched.");
				e.printStackTrace();
				return;
			}
		}

		try {
			IdPatchCli cli = new IdPatchCli();
			cli.Start(esFiles);
		} catch (IOException e) {
			System.out.println("An IO error occured while trying to patch the files.");
			e.printStackTrace();
			return;
		}

	}

	public void Start(List<EsFile> esFiles) {

		Runnable runnable = new Runnable() {
			public void run() {
				try {
					filePatcher.patchFiles(esFiles, translations, "Tamriel_Data.esm", 7501911, true);
				} catch (JSQLParserException e) {
					System.out.println("An error occured while trying to patch the files.");
					e.printStackTrace();
				}
			}
		};

		Thread thread = new Thread(runnable);
		thread.setPriority(Thread.MAX_PRIORITY);
		thread.start();
	}

	public synchronized void notifyAboutCurrentFile(String currentFile) {
		System.out.println("Patching "+currentFile+"...");
	}

	public synchronized void notifyAboutProgress(int percent, boolean finished) {
		if (!started) {
			System.out.print("Patching");
			this.progress = percent;
			this.started = true;
		} else if (!finished && percent != this.progress) {
			System.out.print(".");
			this.progress = percent;
		} else if (finished) {
			System.out.println("DONE!");
		}
	}

	public synchronized void notifyAboutLastObject(String object) {
	}

}
