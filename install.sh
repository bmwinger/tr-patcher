#!/bin/bash

if [[ $# < 1 ]]; then
    echo "Usage: $0 INSTALL_PARENT LIB_PATH
Note that the install parent should be a directory into which the following structure will be installed
INSTALL_PARENT/share/java/tr-patcher/tr-patcher.jar
INSTALL_PARENT/share/java/tr-patcher/Translation.txt
INSTALL_PARENT/bin/tr-patcher
Generally this would be one of /usr, /usr/local, ~/.local

LIB_PATH is optional (defaults to share/java/tr-patcher) and is the path within INSTALL_PARENT where the
library jar is installed
"
    exit
else
   directory=$1
   if [[ $# == 2 ]]; then
      LIB_PATH=$2
   else
      LIB_PATH=share/java/tr-patcher
   fi
fi

gradle build

version=$(basename `pwd`)

mkdir -p "${directory}/${LIB_PATH}"
mkdir -p "${directory}/bin"
TMPDIR=`mktemp -d`
ZIP=`realpath ./build/distributions/$version.zip`
unzip -d "${TMPDIR}" $ZIP

sed -i -e "s,\$APP_HOME\/lib,\$APP_HOME\/${LIB_PATH},g" "${TMPDIR}/$version/bin/$version"

cp ${TMPDIR}/$version/lib/* "${directory}/${LIB_PATH}"
cp "${TMPDIR}/$version/bin/$version" "${directory}/bin/tr-patcher"

rm -r "${TMPDIR}"
