# Tamriel Data Filepatcher

This is an adaptation of the Tamriel Data Filepatcher, originally an Eclipse project written in Java by the Tamriel Rebuilt Team and originally distributed [on Nexus Mods](https://www.nexusmods.com/morrowind/mods/44537/).

## Dependencies
The tr-patcher depends on two jar files that are fetched by gradle and installed alongside the tr-patcher jar file.

- JSqlParser (licensed under Apache License v2.0, or GPL 2.0, LGPL 2.0 and later)
- Jep Java (licensed under GPL 3.0).

The tr-patcher is licensed under the GPL 3.0 (a copy is provided in the root directory of this archive).

## Building

Building requires gradle, and tr-patcher can be built by running `gradle build` within the source directory.
This will produce archives within the `build/distributions` directory, which can be extracted to the desired installation tree
(e.g. with `unzip build/distributions/tr-patcher-ver.zip -d /usr/local/`)

An install.sh script is provided which can be used to automatically install into a custom directory. By default, gradle stores the jar files in lib (or /usr/lib, if you install into /usr), which is not usually the desired location on most linux systems.

For example, to install binaries into /usr/bin and jars into /usr/share/tr-patcher/lib, run `./install.sh /usr share/tr-patcher/lib`.

## Function
The filepatcher reads a translation.txt, which needs to be in the same directory as the filepatcher jar, and replaces IDs in
ESP (Elder Scrolls Plugin) and ESM (Elder Scrolls Master) files for The Elder Scrolls III: Morrowind.

The only IDs it cannot replace are LTEX (local textures) and IDs within compiled scripts (simply recompiling all scripts will solve the later problem).
It cannot repair references or automatically redirect them.

## Terms and Conditions

tr-patcher is provided as-is, without further support or warranty for life, limb, and file integrity, in the hope that someone else will find it useful and extend on its functionality.
